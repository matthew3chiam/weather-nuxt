export const state = () => ({
  list: [],
  location: {}
})

export const mutations = {
  add(state, weather) {
    state.list.push(weather)
  },
  remove(state, e) {
    state.list.splice(e, 1)
  },
  toggle(state, todo) {
    todo.done = !todo.done
  },
  addLocation(state, location) {
    state.location = location
  },
}